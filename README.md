# README #

### What is this repository for? ###

This repository contains a simple example showing how to integrate [Hystrix](https://github.com/Netflix/Hystrix) circuit breaker with a Spring Boot application.

WARNING: The aplication is not production-ready (eg. all the returned values are hard coded)

### How do I get set up? ###

The application consists of two services communicating with each other:

1. [The main service](https://bitbucket.org/vessla/demo-hystrix/src/f4c6f63f1a5c12cb630245ba333d60059485410a/demo-hystrix-service/?at=master) which may fail (its API is called using Hystix commands)
2. [The client service](https://bitbucket.org/vessla/demo-hystrix/src/f4c6f63f1a5c12cb630245ba333d60059485410a/demo-hystrix-client/?at=master) which attempts to communicate with the main service

Assuming, that both services run on localhost, the client service is available at http://localhost:8090/to-read 

While the main service is running, the client service should return "Spring in Action (Manning), Cloud Native Java (O'Reilly), Learning Spring Boot (Packt)". When the main service is down, the client service should return "Cloud Native Java (O'Reilly)".

The Hystrix dashboard is available at http://localhost:8090/hystrix 

The Hystrix stream for the client service is available at http://localhost:8090/hystrix.stream 
(for multiple Hystrix endpoints use Netflix Turbine to aggregate the events from different sources)

### Useful links regarding Hystrix ###

1. ["Application Resilience Engineering and Operations at Netflix with Hystrix"](https://www.youtube.com/watch?v=RzlluokGi1w)

### Who do I talk to? ###

pjadamska@gmail.com
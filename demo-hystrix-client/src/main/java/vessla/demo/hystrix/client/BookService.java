package vessla.demo.hystrix.client;

import java.net.URI;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@Service
public class BookService {


	  private final RestTemplate restTemplate;

	  public BookService(RestTemplate rest) {
	    this.restTemplate = rest;
	  }

	  @HystrixCommand(fallbackMethod = "reliable", 
			  commandProperties = {
					  @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000"),
					  // defines the time interval after which the request to the remote service will be resumed
					  @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
					  // defines the number of request errors after which the circuit breaker will trip open
					  // (in this case, after two unsuccessful requests, the 3rd request will not even hit the
					  // endpoint - the method will return null)
					  @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "1")
	  		  },
			  /*
			   * When a remote service starts to respond slowly, a typical application will continue to call that 
			   * remote service.
			   * The application doesn’t know if the remote service is healthy or not and new threads are spawned 
			   * every time a request comes in. This will cause threads on an already struggling server to be used.
			   * 
			   * (see: http://www.baeldung.com/introduction-to-hystrix )
			   */
			  threadPoolProperties = {
					  // number of threads that always stay alive in the thread pool
					  @HystrixProperty(name = "coreSize", value = "3"), 
					  // Hystrix will start rejecting the requests when the maximum number of threads have reached 10 
					  // and the task queue has reached a size of 10
					  @HystrixProperty(name = "maxQueueSize", value = "1"), 
					  @HystrixProperty(name = "queueSizeRejectionThreshold", value = "1")
			  }
	  )
	  public String readingList() {
		// Invoking the external service here...
	    URI uri = URI.create("http://localhost:8080/recommended");

	    return this.restTemplate.getForObject(uri, String.class);
	  }

	  /*
	   * The @HystrixCommand annotation has reliable as its fallbackMethod, so if, for some reason, Hystrix opens the 
	   * circuit on readingList(), we’ll have an excellent (if short) placeholder reading list ready for our users.
	   * 
	   * (see https://spring.io/guides/gs/circuit-breaker/)
	   */
	  public String reliable() {
	    return "Cloud Native Java (O'Reilly)";
	  }

}
